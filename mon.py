import hive
import json
import requests


KEYWORDS = ['nibir', 'mamun-raihan']

RQ_SESSION = None


def _session():
    return requests.Session()


def _clear_session():
    global RQ_SESSION
    RQ_SESSION = _session()


def _call_additional(domain_json):
    global RQ_SESSION
    config = hive.get_config()
    url = config.get('additional_url')
    assert 'my.freenom.com' in url
    domain = domain_json.get('domain')
    tld = domain_json.get('tld')
    data = {'domain': domain, 'tld': tld}
    response = RQ_SESSION.post(url, data=data, verify=False)
    response_json = json.loads(response.content.decode('utf-8'))
    if response_json.get('available') != 1:
        print(domain+tld+' just went missing.........')


def _update_period(domain_json):
    global RQ_SESSION
    config = hive.get_config()
    url = config.get('period_update_url')
    assert 'my.freenom.com' in url
    domain = domain_json.get('domain')
    tld = domain_json.get('tld')
    period = domain_json.get('target_period')
    data = {'domain': domain, 'period': period}
    response = RQ_SESSION.post(url, data=data, verify=False)
    response_json = json.loads(response.content.decode('utf-8'))
    if response_json.get('status') == 'OK':
        print(domain+tld+' period updated!')
    else:
        print('Could not update period for '+domain+tld)


def main():
    global RQ_SESSION
    RQ_SESSION = _session()
    domains = hive.free_domains(KEYWORDS)
    for domain in domains:
        _call_additional(domain)
    print(dict(RQ_SESSION.cookies))
    
    for domain in domains:
        _update_period(domain)
    print(dict(RQ_SESSION.cookies))


if __name__ == '__main__':
    main()