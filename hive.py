import json
import urllib3
import requests

from functools import lru_cache

urllib3.disable_warnings()
CONFIG_PATH = 'config.json'


@lru_cache(maxsize=None)
def get_config():
    config = json.load(open(CONFIG_PATH, 'r'))
    return config


def _parsed(binary, is_json=True):
    if is_json is True:
        return json.loads(binary.decode('utf-8'))


def _domain_filter(domain_json, tld='.tk'):
    is_free = domain_json.get('type') == 'FREE'
    is_available = domain_json.get('status') == 'AVAILABLE'
    is_tld = domain_json.get('tld') == tld
    return is_free and is_available and is_tld


def extract_domain(response_b):
    config = get_config()
    tld = config.get('tld') if config.get('tld') else '.tk'
    response_json = _parsed(response_b)
    assert response_json.get('status') == "OK"
    free_domains = response_json.get('free_domains')
    filterd = filter(lambda domain: _domain_filter(domain, tld), free_domains)
    return list(filterd)


def av_domain(keyword):
    config = get_config()
    url = config.get('query_url')
    data = {'domain': keyword, 'tld': ''}
    response = requests.post(url, data=data, verify=False)
    # iterable filterd(by tld) free domain
    free_domain = extract_domain(response.content)
    if(free_domain == []):
        return None
    return free_domain[0]


def free_domains(keywords):
    """
    returns list of found domains,
    sample format [{'domain': 'mamun-nibir', 'tld': '.tk'}...]
    """
    domain_list = []
    for keyword in keywords:
        free_domain = av_domain(keyword.strip())
        if free_domain:
            name = free_domain.get('domain')
            tld = free_domain.get('tld')
            domain = {'domain': name, 'tld': tld}
            domain_list.append(domain)
    return domain_list


if __name__ == '__main__':
    import sys
    keywords = sys.argv[1:]
    domains = free_domains(keywords)
    print(domains)