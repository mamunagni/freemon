import requests
from html.parser import HTMLParser


class FreeHtmlParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'input':
            for attr in attrs:
                input_type, input_val = attr
                if input_type == 'name' and input_val == 'token':
                    self.token_hive = attrs


def main():
    url = 'https://my.freenom.com/cart.php?a=checkout'
    res = requests.get(url, verify=False)
    p = FreeHtmlParser()
    p.feed(res.content.decode('utf-8'))
    print(p.token_hive)

if __name__ == '__main__':
    main()